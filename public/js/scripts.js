/**
 * Created by Gianfranco on 14/02/2016.
 */
function desempeno(i,j){
    valor = document.getElementById('desempeno['+i+']['+j+']').value;
    if(valor == 0){
        document.getElementById('desempeno['+i+']['+j+']').value = "";
        alert('El elemento de la matriz de desempeño A'+(i+1)+' C'+(j+1)+' no puede ser 0, porque generaría indeterminación');
        document.getElementById('desempeno['+i+']['+j+']').focus();
    }
}
function valida(parametro,i){
    switch (parametro){
        case 'peso':
            valor = document.getElementById('peso-'+i).value;
            if(isNaN(valor) || valor <= 0){
                document.getElementById('peso-'+i).value = "";
                msjAlert('Peso');
            }
            document.getElementById('peso-'+i).focus();
            break;
        case 'indiferencia':
            valor = document.getElementById('indiferencia-'+i).value;
            if(isNaN(valor) || valor <= 0){
                document.getElementById('indiferencia-'+i).value = "";
                msjAlert('Indiferencia');
            }
            document.getElementById('indiferencia-'+i).focus();
            break;
        case 'preferencia':
            valor = document.getElementById('preferencia-'+i).value;
            if(isNaN(valor) || valor <= 0){
                document.getElementById('preferencia-'+i).value = "";
                msjAlert('Preferencia');
            }
            document.getElementById('preferencia-'+i).focus();
            break;
        case 'veto':
            valor = document.getElementById('veto-'+i).value;
            if(isNaN(valor) || valor <= 0){
                document.getElementById('veto-'+i).value = "";
                msjAlert('Veto');
            }
            document.getElementById('veto-'+i).focus();
            break;
    }
}

function msjAlert(etiqueta){
    alert('El elemento '+etiqueta+' ingresado debe ser numérico y mayor a 0');
}

function verifica(criterio,alternativa){
    criterio = parseInt(criterio);
    alternativa = parseInt(alternativa);

    var indiferencia = document.getElementsByClassName('indiferencia');
    var preferencia = document.getElementsByClassName('preferencia');
    var veto = document.getElementsByClassName('veto');

    var pase_a = true;
    var pase_b = true;
    var pase_c = true;

    for(var j = 0;j < alternativa;j++){
        for(var k = 0;k < criterio;k++){
            var desempeno = document.getElementById('desempeno['+j+']['+k+']').value;
            if(desempeno.length == 0){
                alert("Faltan elementos en la matriz de desempeño: A"+(j+1)+" C"+(k+1));
                document.getElementById('desempeno['+j+']['+k+']').focus();
                k = criterio;
                j = alternativa;
                pase_a = false;
            }
        }
    }

    if(pase_a === true){
        for(var p = 0;p < criterio; p++){
            peso = document.getElementById('peso-'+p).value;
            if(isNaN(peso) || peso.length == 0 || peso == "" || peso == null){
                alert("Falta el peso del criterio "+(p+1));
                document.getElementById('peso-'+p).focus();
                pase_b = false;
                p = criterio;
            }
        }
        if(pase_b == true){

            for(var i = 0; i < criterio; i++){

                if((indiferencia[i].value).length > 0 || indiferencia[i].value != "" || indiferencia[i].value != null){
                    if((preferencia[i].value).length > 0 || preferencia[i].value != "" || preferencia[i].value != null){

                        if(parseFloat(indiferencia[i].value) < parseFloat(preferencia[i].value) && parseFloat(indiferencia[i].value) != parseFloat(preferencia[i].value)){

                            if((preferencia[i].value).length != 0 || preferencia[i].value != "" || preferencia[i].value != null){
                                if((veto[i].value).length != 0 || veto[i].value != "" || veto[i].value != null){

                                    //if(parseFloat(preferencia[i].value) < parseFloat(veto[i].value) && parseFloat(preferencia[i].value) != parseFloat(veto[i].value)){
                                    if(parseFloat(preferencia[i].value) <= parseFloat(veto[i].value)){
                                        pase_c = true;
                                    }else{
                                        pase_c = false;
                                        alert("Los umbrales del criterio "+(i+1)+" no cumplen con la definicion ( 'q' es menor que 'p' es menor o igual que 'v' )");
                                        i = criterio;
                                    }

                                }else{
                                    pase_c = false;
                                    alert("El veto del criterio "+i+" esta vacio");
                                    document.getElementById('veto-'+i).focus();
                                    i = criterio;
                                }
                            }else{
                                pase_c = false;
                                alert("La preferencia del criterio "+i+" esta vacio");
                                document.getElementById('preferencia-'+i).focus();
                                i = criterio;
                            }

                        }else{

                            pase_c = false;
                            alert("Los umbrales del criterio "+(i+1)+" no cumplen con la definicion ( 'q' es menor que 'p' es menor o igual que 'v' )");
                            i = criterio;

                        }

                    }else{

                        pase_c = false;
                        alert("La preferencia del criterio "+i+" esta vacio");
                        document.getElementById('preferencia-'+i).focus();
                        i = criterio;
                    }

                }else{

                    pase_c = false;
                    alert("La indiferencia del criterio "+i+" esta vacio");
                    document.getElementById('indiferencia-'+i).focus();
                    i = criterio;

                }

            }

            if(pase_c == true){
                document.getElementById('formCargarDatos').submit();
            }

        }
    }
}