@extends('template.master')
@section('region_editable')
    <h2 class="text-center">Editar Proyecto</h2>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#titulo" data-toggle="tab" aria-expanded="false">Título & Coeficientes de Destilación</a></li>
        <li class=""><a href="#criterio" data-toggle="tab" aria-expanded="true">Criterios & Alternativas</a></li>
        <li class=""><a href="#matriz" data-toggle="tab" aria-expanded="true">Matriz de Desempeño</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="titulo">
            <p>
                <form class="form-horizontal" action="{{ route('actualizarProyecto',$proyecto->id) }}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <h4>¿Desea cambiar el título del proyecto?</h4>
                    <input class="form-control" name="Titulo" value="{{ $proyecto->titulo }}" required autofocus>
                    <hr>
                    <h4>¿Desea cambiar los coeficientes de destilación?</h4>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="Alfa">Coeficiente Alfa</label>
                            <input type="number" step="any" class="form-control text-center" name="Alfa" placeholder="Ingrese &alpha;" value="{{ $proyecto->alfa }}" required>
                        </div>
                        <div class="col-lg-6">
                            <label for="Beta">Coeficiente Beta</label>
                            <input type="number" step="any" class="form-control text-center" name="Beta" placeholder="Ingrese &beta;" value="{{ $proyecto->beta }}" required>
                        </div>
                    </div>
                    <br>
                    <div class="text-center"><button type="submit" class="btn btn-success">Actualizar</button></div>
                </form>
            </p>
        </div>
        <div class="tab-pane fade" id="criterio">
            <p>

            <form class="form-horizontal" action="{{ route('actualizarElemento') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id_proyecto" value="{{ $proyecto->id }}">

                <h4>¿Desea editar los criterios y alternativas?</h4>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <fieldset>
                            <h2 class="text-center">Criterios</h2>
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th><div class="text-center">Cod.</div></th>
                                    <th><div class="text-center">Etiqueta</div></th>
                                    <th><div class="text-center">Tipo</div></th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 0; $i < $proyecto->criterio; $i++)
                                    <tr>
                                        <td class="text-center">C{{ $i+1 }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="criterio[{{$i}}]" value="{{ $criterios['C'.$i]['etiqueta'] }}" required>
                                        </td>
                                        <td>
                                            <select class="form-control" name="tipo[{{ $i }}]">
                                                @if($criterios['C'.$i]['tipo'] == 'default')
                                                <option value="default" selected>Ascendente</option>
                                                <option value="inverso">Descendente</option>
                                                @else
                                                <option value="default">Ascendente</option>
                                                <option value="inverso" selected>Descendente</option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                        </fieldset>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <fieldset>
                            <h2 class="text-center">Alternativas</h2>
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th><div class="text-center">Cod.</div></th>
                                    <th><div class="text-center">Etiqueta</div></th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 0; $i < $proyecto->alternativa; $i++)
                                    <tr>
                                        <td class="text-center">A{{ $i+1 }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="alternativa[{{$i}}]" value="{{ $alternativas['A'.$i]['etiqueta'] }}" required>
                                        </td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                        </fieldset>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                        <button type="submit" class="btn btn-success">Actualizar</button>
                    </div>
                </div>

            </form>

            </p>
        </div>
        <div class="tab-pane fade" id="matriz">
            <p>
            <h4>¿Desea editar la matriz de desempeño?</h4>
            <form id="formCargarDatos" class="form-horizontal" action="{{ route('actualizarDatos') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id_proyecto" value="{{ $proyecto->id }}">

                <fieldset>
                <table class="table table-responsive">
                    <thead>
                    <th>&nbsp;</th>
                    @for($i = 0; $i < count($criterios); $i++)
                        <th class="text-center"><div data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $criterios['C'.$i]['etiqueta'] }}">C{{ $i+1 }}</div></th>
                    @endfor
                    </thead>
                    @for($i=0;$i<count($alternativas);$i++)
                        <tr>
                            <td><div data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ $alternativas['A'.$i]['etiqueta'] }}"><strong>A{{ $i+1 }}</strong></div></td>
                            @for($j=0;$j<count($criterios);$j++)
                                @if($i == 0 && $j == 0)
                                    @if($criterios['C'.$j]['tipo'] == 'default')
                                        <td><input type="number" class="form-control text-center" id="desempeno[{{$i}}][{{$j}}]" name="desempeno[{{$i}}][{{$j}}]" value="{{ $desempeno[$i][$j] }}" autofocus required></td>
                                    @else
                                        <td class="warning"><input type="number" class="form-control text-center" onchange="desempeno({{$i}},{{$j}})" id="desempeno[{{$i}}][{{$j}}]" name="desempeno[{{$i}}][{{$j}}]" value="{{ $desempeno[$i][$j] }}" placeholder="1 / x" autofocus required></td>
                                    @endif
                                @else
                                    @if($criterios['C'.$j]['tipo'] == 'default')
                                        <td><input type="number" class="form-control text-center" id="desempeno[{{$i}}][{{$j}}]" name="desempeno[{{$i}}][{{$j}}]" value="{{ $desempeno[$i][$j] }}" required></td>
                                    @else
                                        <td class="warning"><input type="number" class="form-control text-center" onchange="desempeno({{$i}},{{$j}})" id="desempeno[{{$i}}][{{$j}}]" name="desempeno[{{$i}}][{{$j}}]" value="{{ $desempeno[$i][$j] }}" placeholder="1 / x" required></td>
                                    @endif
                                @endif
                            @endfor
                        </tr>
                    @endfor

                    <tr>
                        <th>&nbsp;</th>
                        @for($i=0;$i<$proyecto->criterio;$i++)
                            <th class="text-center">C{{ $i+1 }}</th>
                        @endfor
                    </tr>

                    <tr>
                        <td><strong>Pesos</strong></td>
                        @for($j=0;$j<$proyecto->criterio;$j++)
                        <td class="info">
                            <input type="number" class="form-control text-center peso" name="peso[{{$j}}]" id="peso-{{$j}}" onchange="valida('peso','{{$j}}')" value="{{ $peso[$j] }}" required>
                        </td>
                        @endfor
                    </tr>

                    <tr>
                        <td><strong>Indiferencia (q)</strong></td>
                        @for($j=0;$j<$proyecto->criterio;$j++)
                            <td class="success">
                                <input type="number" class="form-control text-center indiferencia" name="indiferencia[{{$j}}]" id="indiferencia-{{$j}}" onchange="valida('indiferencia','{{$j}}')" value="{{ $indiferencia[$j] }}" required>
                            </td>
                        @endfor
                    </tr>

                    <tr>
                        <td><strong>Preferencia (p)</strong></td>
                        @for($j=0;$j<$proyecto->criterio;$j++)
                            <td class="danger">
                                <input type="number" class="form-control text-center preferencia" name="preferencia[{{$j}}]" id="preferencia-{{$j}}" onchange="valida('preferencia','{{$j}}')" value="{{ $preferencia[$j] }}" required>
                            </td>
                        @endfor
                    </tr>

                    <tr>
                        <td><strong>Veto (v)</strong></td>
                        @for($j=0;$j<$proyecto->criterio;$j++)
                            <td class="danger">
                                <input type="number" class="form-control text-center veto" name="veto[{{$j}}]" id="veto-{{$j}}" onchange="valida('veto','{{$j}}')" value="{{ $veto[$j] }}" required>
                            </td>
                        @endfor
                    </tr>

                </table>

                <div class="form-group">
                    <div class="col-lg-12 text-center">
                        <button type="button" class="btn btn-success" onclick="verifica('{{$proyecto->criterio}}','{{$proyecto->alternativa}}')">Actualizar</button>
                    </div>
                </div>
            </fieldset>

            </form>
            </p>
        </div>
    </div>
@stop