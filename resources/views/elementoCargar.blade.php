@extends('template.master')
@section('region_editable')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h2 class="text-center">{{ $proyecto->titulo }}</h2>
        </div>
        <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12">
            <form class="form-horizontal" action="{{ route('almacenarElemento') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id_proyecto" value="{{ $proyecto->id }}">

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <fieldset>
                            <h2 class="text-center">Criterios</h2>
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th><div class="text-center">Cod.</div></th>
                                        <th><div class="text-center">Etiqueta</div></th>
                                        <th><div class="text-center">Tipo</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i = 0; $i < $proyecto->criterio; $i++)
                                    <tr>
                                        <td class="text-center">C{{ $i+1 }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="criterio[{{$i}}]" required>
                                        </td>
                                        <td>
                                            <select class="form-control" name="tipo[{{ $i }}]">
                                                <option value="default" selected>Ascendente</option>
                                                <option value="inverso">Descendente</option>
                                            </select>
                                        </td>
                                    </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </fieldset>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <fieldset>
                            <h2 class="text-center">Alternativas</h2>
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th><div class="text-center">Cod.</div></th>
                                    <th><div class="text-center">Etiqueta</div></th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 0; $i < $proyecto->alternativa; $i++)
                                    <tr>
                                        <td class="text-center">A{{ $i+1 }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="alternativa[{{$i}}]" required>
                                        </td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                        </fieldset>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@stop