@extends('template.master')
@section('region_editable')
    <h2 class="text-center">{{ $titulo }}</h2><br>
    <table class="table table-responsive">
        <thead>
        <tr class="info">
            <th><div class="text-center">N°</div></th>
            <th><div class="text-center">Título</div></th>
            <th><div class="text-center"># Crit.</div></th>
            <th><div class="text-center"># Alt.</div></th>
            <th><div class="text-center">Creado por</div></th>
            <th><div class="text-center">Acciones</div></th>
        </tr>
        </thead>
        <tbody>
        <?php $n = 1;?>
        @foreach($proyectos as $proyecto)
            <tr class="warning">
                <td><div class="text-center">{{ $n++ }}</div></td>
                <td><div>{{ $proyecto->titulo }}</div></td>
                <td><div class="text-center">{{ $proyecto->criterio }}</div></td>
                <td><div class="text-center">{{ $proyecto->alternativa }}</div></td>
                <td><div class="text-center">{{ $proyecto->users->name }}</div></td>
                <td>
                    <div class="text-center">
                        @if(Auth::check())
                            @if($proyecto->user_id == Auth::user()->id || Auth::user()->role == "admin")
                                <a href="{{ route('mostrarProyecto',$proyecto->id) }}" class="btn btn-xs btn-success">Resultados</a>
                                <a href="{{ route('editarProyecto',$proyecto->id) }}" class="btn btn-xs btn-info">Editar</a>
                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-{{ $proyecto->id }}">Eliminar</button>
                            @else
                                <a href="{{ route('mostrarProyecto',$proyecto->id) }}" class="btn btn-xs btn-success">Resultados</a>
                            @endif
                        @else
                            <a href="{{ route('mostrarProyecto',$proyecto->id) }}" class="btn btn-xs btn-success">Resultados</a>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6">
                    <div class="text-center">
                        {!! $proyectos->render() !!}
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
@stop

@section('modals')
    @foreach($proyectos as $proyecto)
    <!-- Modal -->
    <div class="modal fade" id="modal-{{ $proyecto->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" method="POST" action="{{ route('eliminarProyecto',['id' => $proyecto->id]) }}" accept-charset="utf-8">
                    <input name="_method" type="hidden" value="DELETE"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ $proyecto->titulo }}</h4>
                </div>
                <div class="modal-body">
                    ¿Desea eliminar el proyecto <code>{{ $proyecto->titulo }}</code>?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach
@stop