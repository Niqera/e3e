@extends('template.master')
@section('region_editable')
    <div class="row">
        <div class="col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-12">
            <h2 class="text-center">Registro de usuario</h2>
            <form class="form-horizontal" method="POST" action="{{ route('authPostRegister') }}">
                {!! csrf_field() !!}
                <fieldset>
                    <div class="form-group">
                        <div class="col-lg-12">
                            Nombre
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            Email
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            Contraseña
                            <input type="password" class="form-control" name="password" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            Confirmar Contraseña
                            <input type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-primary">Registrarme</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop