@extends('template.master')
@section('region_editable')
    <div class="row">
        <div class="col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-12">
            <h2 class="text-center">Inicio de sesión</h2>
            <form class="form-horizontal" method="POST" action="{{ route('authPostLogin') }}">
                {!! csrf_field() !!}
                <fieldset>
                    <div class="form-group">
                        <div class="col-lg-12">
                            Email
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            Contraseña
                            <input type="password" class="form-control" name="password" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <input type="checkbox" name="remember"> Recordarme
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop