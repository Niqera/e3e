@extends('template.master')
@section('region_editable')
    <h2 class="text-center">Listado de Usuarios</h2><br>
    <table class="table table-responsive">
        <thead>
            <tr>
                <th><div class="text-center">N°</div></th>
                <th><div class="text-center">Usuario</div></th>
                <th><div class="text-center">Email</div></th>
                <th><div class="text-center">Rol</div></th>
                <th><div class="text-center">Creado el</div></th>
                <th><div class="text-center">Acciones</div></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $a = 1;
            ?>
            @foreach($users as $user)
            <tr>
                <td><div class="text-center">{{ $a++ }}</div></td>
                <td><div>{{ $user->name }}</div></td>
                <td><div class="text-center">{{ $user->email }}</div></td>
                <td><div class="text-center text-capitalize">{{ $user->role }}</div></td>
                <td><div class="text-center">{{ $user->created_at }}</div></td>
                <td>
                    <div class="text-center">
                        <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#roles-{{ $user->id }}">Roles</button>
                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-{{ $user->id }}">Eliminar</button>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="6">
                <div class="text-center">
                    {!! $users->render() !!}
                </div>
            </td>
        </tr>
        </tfoot>
    </table>
@stop

@section('modals')
    @foreach($users as $user)
    <!-- Modal -->
    <div class="modal fade" id="roles-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" method="POST" action="{{ route('editAccountUser',$user->id) }}" accept-charset="utf-8">
                    <input name="_method" type="hidden" value="PUT"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Editar Usuario</h4>
                    </div>
                    <div class="modal-body">
                        ¿Desea cambiar el rol del usuario <code>{{ $user->name }}</code>?
                        <div style="padding-top: 10px;">
                            <select name="role" class="form-control">
                                @if($user->role == "admin")
                                    <option value="user">User</option>
                                    <option value="admin" selected>Admin</option>
                                @else
                                    <option value="user" selected>User</option>
                                    <option value="admin">Admin</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach

    <!--Modales para eliminar-->
    @foreach($users as $user)
    <!-- Modal -->
    <div class="modal fade" id="modal-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" method="POST" action="{{ route('deleteAccountUser',$user->id) }}" accept-charset="utf-8">
                    <input name="_method" type="hidden" value="DELETE"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Usuario</h4>
                    </div>
                    <div class="modal-body">
                        ¿Desea eliminar al usuario <code>{{ $user->name }}</code>?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Eliminar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach
@stop