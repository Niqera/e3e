<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                Electre III
                @if(Auth::check())
                    @if(Auth::user()->role == "admin")
                        - Admin
                    @endif
                @endif
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            @if(Auth::check())
                <ul class="nav navbar-nav">
                    <li {{ (Request::is('/')) ? 'class=active' : '' }}><a href="{{ route('home') }}">Inicio </a></li>
                    <li {{ (Request::is('crearProyecto')) ? 'class=active' : '' }}><a href="{{ route('crearProyecto') }}">Crear Proyecto <span class="sr-only">(current)</span></a></li>
                    <li {{ (Request::is('misProyectos')) ? 'class=active' : '' }}><a href="{{ route('misProyectos') }}">Mis proyectos </a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Bienvenido, {{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('account') }}">Editar Cuenta</a></li>
                            @if(Auth::check())
                                @if(Auth::user()->role == "admin")
                                    <li class="divider"></li>
                                    <li><a href="{{ route('listAccount') }}">Ver Usuarios</a></li>
                                @endif
                            @endif
                            <li class="divider"></li>
                            <li><a href="{{ route('authLogout') }}">Cerrar Sesión</a></li>
                        </ul>
                    </li>
                </ul>

            @else
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('authLogin') }}">Iniciar Sesión</a></li>
                <li><a href="{{ route('authRegister') }}">Regístrate</a></li>
            </ul>
            @endif
        </div>

    </div>
</nav>