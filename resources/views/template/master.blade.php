<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Electre III</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="https://bootswatch.com/united/bootstrap.css" media="screen">
    <link rel="stylesheet" href="https://bootswatch.com/assets/css/custom.min.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://bootswatch.com/bower_components/html5shiv/dist/html5shiv.js"></script>
    <script src="https://bootswatch.com/bower_components/respond/dest/respond.min.js"></script>
    <![endif]-->
</head>
<style>
    body{margin-top: 10px;}
</style>
<body>
@include('template.nav')
<div class="container-fluid">
    @include('alerts.validationErrors')
    @include('alerts.alert-success')
    @include('alerts.alert-warning')
    @include('alerts.alert-danger')
    @yield('region_editable')
    @yield('modals')
</div>
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="https://bootswatch.com/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="https://bootswatch.com/assets/js/custom.js"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
</body>
</html>