@extends('template.master')
@section('region_editable')
    <h2 class="text-center">{{ $proyecto->titulo }}</h2><br>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#desempeno" data-toggle="tab" aria-expanded="false">Desempeño (g)</a></li>
        <li class=""><a href="#concordancia" data-toggle="tab" aria-expanded="true">Concordancia (C)</a></li>
        <li class=""><a href="#discordancia" data-toggle="tab" aria-expanded="true">Discordancia (D)</a></li>
        <li class=""><a href="#credibilidad" data-toggle="tab" aria-expanded="true">Credibilidad (S)</a></li>
        <li class=""><a href="#destilado" data-toggle="tab" aria-expanded="true">Destilación (T)</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="desempeno">
            <p>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th></th>
                    @for($i=0; $i < count($criterios); $i++)
                        <th class="text-center"><div data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $criterios['C'.$i]['etiqueta'] }}">C{{ $i+1 }}</div></th>
                    @endfor
                </tr>
                </thead>
                <tbody>
                @for($i=0; $i < $proyecto->alternativa; $i++)
                    <tr>
                        <td><div data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ $alternativas['A'.$i]['etiqueta'] }}"><strong>A{{ $i+1 }}</strong></div></td>
                        @for($j=0; $j < $proyecto->criterio; $j++)
                            @if($criterios['C'.$j]['tipo'] == 'default')
                            <td class="text-center">{{ $desempeno[$i][$j] }}</td>
                            @else
                            <td class="text-center warning">1/{{ $desempeno[$i][$j] }}</td>
                            @endif
                        @endfor
                    </tr>
                @endfor
                <tr class="info">
                    <td><strong>Pesos</strong></td>
                    @for($j=0; $j < $proyecto->criterio; $j++)
                        <td class="text-center">{{ $peso[$j] }}</td>
                    @endfor
                </tr>
                <tr class="success">
                    <td><strong>Indiferencia (q)</strong></td>
                    @for($j=0; $j < $proyecto->criterio; $j++)
                        <td class="text-center">{{ $indiferencia[$j] }}</td>
                    @endfor
                </tr>
                <tr class="danger">
                    <td><strong>Preferencia (p)</strong></td>
                    @for($j=0; $j < $proyecto->criterio; $j++)
                        <td class="text-center">{{ $preferencia[$j] }}</td>
                    @endfor
                </tr>
                <tr class="warning">
                    <td><strong>Veto (v)</strong></td>
                    @for($j=0; $j < $proyecto->criterio; $j++)
                        <td class="text-center">{{ $veto[$j] }}</td>
                    @endfor
                </tr>
                </tbody>
            </table>
            </p>
        </div>
        <div class="tab-pane fade" id="concordancia">
            <p>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th></th>
                    @for($i=0; $i < count($alternativas); $i++)
                        <th class="info"><div class="text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $alternativas['A'.$i]['etiqueta'] }}"><strong>P{{ $i+1 }}</strong></div></th>
                    @endfor
                </tr>
                </thead>
                <tbody>
                @for($i=0; $i < count($alternativas); $i++)
                    <tr>
                        <td class="info"><div data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ $alternativas['A'.$i]['etiqueta'] }}"><strong>P{{ $i+1 }}</strong></div></td>
                        @for($j=0; $j < $proyecto->alternativa; $j++)
                            @if($i == $j)
                            <td><div class="text-center">{{ $concordancia[$i][$j] }}</div></td>
                            @else
                            <td><div class="text-center">{{ number_format((float)$concordancia[$i][$j],2) }}</div></td>
                            @endif
                        @endfor
                    </tr>
                @endfor
                </tbody>
            </table>
            </p>
        </div>
        <div class="tab-pane fade" id="discordancia">
            <p>
            <table class="table table-responsive">
                <thead>
                <th></th>
                @for($i=0; $i < count($alternativas); $i++)
                    <th class="info"><div class="text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $alternativas['A'.$i]['etiqueta'] }}"><strong>P{{ $i+1 }}</strong></div></th>
                @endfor
                </thead>
                <tbody>
                @for($i=0; $i < count($alternativas); $i++)
                    <tr>
                        <td class="info"><div data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ $alternativas['A'.$i]['etiqueta'] }}"><strong>P{{ $i+1 }}</strong></div></td>
                        @for($j=0; $j < $proyecto->alternativa; $j++)
                            @if(count($discordancia[$i][$j]) == 1)
                                <td><div class="text-center">{{ $discordancia[$i][$j] }}</div></td>
                            @else
                                <?php $aux = $discordancia[$i][$j];?>
                                <td>
                                    <div class="text-center">
                                        [
                                        @for($k=0;$k<count($aux);$k++)
                                            {{ number_format($aux[$k],2) }}
                                            @if($k != count($aux)-1)
                                                -
                                            @endif
                                        @endfor
                                        ]
                                    </div>
                                </td>
                            @endif
                        @endfor
                    </tr>
                @endfor
                </tbody>
            </table>
            </p>
        </div>
        <div class="tab-pane fade" id="credibilidad">
            <p>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th></th>
                    @for($i=0; $i < count($alternativas); $i++)
                        <th class="success"><div class="text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $alternativas['A'.$i]['etiqueta'] }}"><strong>P{{ $i+1 }}</strong></div></th>
                    @endfor
                </tr>
                </thead>
                <tbody>
                @for($i=0; $i < count($alternativas); $i++)
                    <tr>
                        <td class="success"><div data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ $alternativas['A'.$i]['etiqueta'] }}"><strong>P{{ $i+1 }}</strong></div></td>
                        @for($j=0; $j < $proyecto->alternativa; $j++)
                            @if($i == $j)
                            <td><div class="text-center">{{ $credibilidad[$i][$j] }}</div></td>
                            @else
                            <td><div class="text-center">{{ number_format((float)$credibilidad[$i][$j],2) }}</div></td>
                            @endif
                        @endfor
                    </tr>
                @endfor
                </tbody>
            </table>
            </p>
        </div>

        <div class="tab-pane fade in" id="destilado">
            <div class="row">
                <div class="col-lg-4">
                    <table class="table table-responsive table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2" class="text-center">Calificaciones - Primer Destilado</th>
                            </tr>
                            <tr>
                                <th><div class="text-center">Proyecto</div></th>
                                <th><div class="text-center">Calificación Neta (Qn)</div></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($primer_destilado as $pd)
                            <tr>
                                <td><div>Proyecto {{ $pd['proyecto'] + 1 }}</div></td>
                                <td><div class="text-center">{{ $pd[0] }}</div></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-4">
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th colspan="3" class="text-center">Ranking</th>
                        </tr>
                        <tr>
                            <th><div class="text-center">Posición</div></th>
                            <th><div class="text-center">Destilado Descendente</div></th>
                            <th><div class="text-center">Qn</div></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $aux = 1;
                        ?>
                        @for($i = 0; $i < count($ranking_descendente); $i++)
                            <tr>
                                <td>
                                    <div class="text-center">
                                        {{ $aux }}
                                    </div>
                                </td>
                                <td>
                                    <div class="text-center">
                                        @if(count($ranking_descendente[$i]) > 1)
                                            <ul class="list-group list-unstyled">
                                                @for($j = 0; $j < count($ranking_descendente[$i]); $j++)
                                                    <li>{{ "Proyecto ".($ranking_descendente[$i][$j]['proyecto']+1) }}</li>
                                                    <?php $aux++; ?>
                                                @endfor
                                            </ul>
                                        @else
                                            Proyecto {{ $ranking_descendente[$i][0]['proyecto']+1 }}
                                            <?php $aux++; ?>
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    <div class="text-center">
                                        @if(count($ranking_descendente[$i]) > 1)
                                            <ul class="list-group list-unstyled">
                                                @for($j = 0; $j < count($ranking_descendente[$i]); $j++)
                                                    <li>{{($ranking_descendente[$i][$j][0]) }}</li>
                                                @endfor
                                            </ul>
                                        @else
                                            {{ $ranking_descendente[$i][0][0] }}
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-4">
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th colspan="3" class="text-center">Destilado Ascendente</th>
                        </tr>
                        <tr>
                            <th><div class="text-center">Posición</div></th>
                            <th><div class="text-center">Ranking</div></th>
                            <th><div class="text-center">Qn</div></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $aux = 1;
                        ?>
                        @for($i = 0; $i < count($ranking_ascendente); $i++)
                            <tr>
                                <td>
                                    <div class="text-center">
                                        {{ $aux }}
                                    </div>
                                </td>
                                <td>
                                    <div class="text-center">
                                        @if(count($ranking_ascendente[$i]) > 1)
                                            <ul class="list-group list-unstyled">
                                                @for($j = 0; $j < count($ranking_ascendente[$i]); $j++)
                                                    <li>{{ "Proyecto ".($ranking_ascendente[$i][$j]['proyecto']+1) }}</li>
                                                    <?php $aux++; ?>
                                                @endfor
                                            </ul>
                                        @else
                                            Proyecto {{ $ranking_ascendente[$i][0]['proyecto']+1 }}
                                            <?php $aux++; ?>
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    <div class="text-center">
                                        @if(count($ranking_ascendente[$i]) > 1)
                                            <ul class="list-group list-unstyled">
                                                @for($j = 0; $j < count($ranking_ascendente[$i]); $j++)
                                                    <li>{{($ranking_ascendente[$i][$j][0]) }}</li>
                                                @endfor
                                            </ul>
                                        @else
                                            {{ $ranking_ascendente[$i][0][0] }}
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop