@extends('template.master')
@section('region_editable')

    <h3>{{ $proyecto->titulo }}: <small>Matríz de Desempeño, Pesos y Umbrales</small></h3>
    <div class="row">
        <div class="col-lg-12">
            <form id="formCargarDatos" class="form-horizontal" action="{{ route('almacenarDatos') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id_proyecto" value="{{ $proyecto->id }}">
                <fieldset>
                    <table class="table table-responsive">
                        <thead>
                        <th>&nbsp;</th>
                        @for($i = 0; $i < count($criterios); $i++)
                        <th class="text-center"><div data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $criterios['C'.$i]['etiqueta'] }}">C{{ $i+1 }}</div></th>
                        @endfor
                        </thead>
                        @for($i=0;$i<count($alternativas);$i++)
                            <tr>
                                <td><div data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ $alternativas['A'.$i]['etiqueta'] }}"><strong>A{{ $i+1 }}</strong></div></td>
                                @for($j=0;$j<count($criterios);$j++)
                                    @if($i == 0 && $j == 0)
                                        @if($criterios['C'.$j]['tipo'] == 'default')
                                            <td><input type="number" class="form-control text-center" id="desempeno[{{$i}}][{{$j}}]" name="desempeno[{{$i}}][{{$j}}]" value="" autofocus required></td>
                                        @else
                                            <td class="warning"><input type="number" class="form-control text-center" onchange="desempeno({{$i}},{{$j}})" id="desempeno[{{$i}}][{{$j}}]" name="desempeno[{{$i}}][{{$j}}]" value="" placeholder="1 / x" autofocus required></td>
                                        @endif
                                    @else
                                        @if($criterios['C'.$j]['tipo'] == 'default')
                                            <td><input type="number" class="form-control text-center" id="desempeno[{{$i}}][{{$j}}]" name="desempeno[{{$i}}][{{$j}}]" value="" required></td>
                                        @else
                                            <td class="warning"><input type="number" class="form-control text-center" onchange="desempeno({{$i}},{{$j}})" id="desempeno[{{$i}}][{{$j}}]" name="desempeno[{{$i}}][{{$j}}]" value="" placeholder="1 / x" required></td>
                                        @endif
                                    @endif
                                @endfor
                            </tr>
                        @endfor

                        <tr>
                            <th>&nbsp;</th>
                            @for($i=0;$i<$proyecto->criterio;$i++)
                                <th class="text-center">C{{ $i+1 }}</th>
                            @endfor
                        </tr>
                        <?php
                        $arreglos = ['peso','indiferencia','preferencia','veto'];
                        $colores = ['info','success','danger','warning'];
                        $etiquetas = ['Pesos','Indiferencia (q)', 'Preferencia (p)', 'Veto (v)'];
                            $x = 12;
                        ?>
                        @for($i=0;$i<count($etiquetas);$i++)
                            <tr>
                                <td>{{ $etiquetas[$i] }}</td>
                                @for($j=0;$j<$proyecto->criterio;$j++)
                                    <td class="{{ $colores[$i] }}">
                                        <input type="number" class="form-control text-center {{$arreglos[$i]}}" name="{{$arreglos[$i]}}[{{$j}}]" id="{{$arreglos[$i]}}-{{$j}}" onchange="valida('{{ $arreglos[$i]}}','{{$j}}')" value="" required>
                                    </td>
                                @endfor
                            </tr>
                        @endfor
                    </table>

                    <div class="form-group">
                        <div class="col-lg-12 text-center">
                            <button type="button" class="btn btn-primary" onclick="verifica('{{$proyecto->criterio}}','{{$proyecto->alternativa}}')">Guardar</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop