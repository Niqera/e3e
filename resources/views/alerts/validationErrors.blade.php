@if($errors->has())
<div class="alert alert-dismissible alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Oh! </strong><span>@foreach ($errors->all() as $error){{ $error }} @endforeach</span>
</div>
@endif