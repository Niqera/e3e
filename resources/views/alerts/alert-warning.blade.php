@if(Session::has('alert-warning'))
    <div class="alert alert-dismissible alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <div>{{ Session::get('alert-warning') }}</div>
    </div>
@endif