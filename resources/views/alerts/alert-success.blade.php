@if(Session::has('alert-success'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <div>{{ Session::get('alert-success') }}</div>
    </div>
@endif