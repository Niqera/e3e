@extends('template.master')
@section('region_editable')
    <div class="row">
        <div class="col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-12">
            <form class="form-horizontal" action="{{ route('almacenarProyecto') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <fieldset>
                    <h2 class="text-center">Crear Proyecto</h2><br>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <input type="text" class="form-control text-center" name="Titulo" placeholder="Título de Proyecto" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <input type="number" class="form-control text-center" name="Criterio" placeholder="Ingrese # de Criterios" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <input type="number" class="form-control text-center" name="Alternativa" placeholder="Ingrese # de Alternativas" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="text-center"><label for="Alfa">Coeficiente Alfa</label></div>
                        <div class="col-lg-12">
                            <input type="number" step="any" class="form-control text-center" name="Alfa" placeholder="Ingrese &alpha;" value="-0.15" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="text-center"><label for="Beta">Coeficiente Beta</label></div>
                        <div class="col-lg-12">
                            <input type="number" step="any" class="form-control text-center" name="Beta" placeholder="Ingrese &beta;" value="0.30" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop