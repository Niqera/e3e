<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Electre III</title>
</head>
<body style="margin:0;padding:0;font-family:sans-serif;background:#eeeeee;color:#333333;">
<div style="max-width:600px;margin-bottom:10px;margin-top:0px;margin-left:auto;margin-right:auto;background:#ffffff;">
    <div style="background-color: #DD4814; border-bottom:#eeeeee 1px solid;padding-top:5px;text-align: center;">
        <h1 style="color:#FFFFFF; font-weight: lighter;">Electre III</h1>
    </div>
    <div style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;">
        <h2 style="text-align: center; margin:0px; padding-bottom: 0px;">
            Bienvenido {{ $data['name'] }}
        </h2>
        <div style="text-align: center">
            <h3 style="margin-top: 0;padding-top: 0;">Confirme su cuenta dando click en el siguiente link</h3>
            <div><a href="{{ route('authConfirmation',$data['email']) }}">Click aquí y confirme su correo</a></div><br>
            <p style="font-size: 10px;">Responsable del proyecto: Lic. Carlos Alva</p>
        </div>
    </div>
</div>
<div style="max-width:600px;color:#888888;margin-left:auto;margin-right:auto;font-size:10px;padding:0 10px;margin-bottom:30px;text-align:center;">
    <p>
        El presente sistema esta automatizado, por lo que le pedimos no responda al presente email. El sistema fue diseñado e implementado por el <a href="https://www.facebook.com/gianfranco.niquin">Ing. Henry Gianfranco Niquín Herrera</a> bajo la supervisión de responsable del proyecto Lic. Carlos Alva.
    </p>
</div>
</body>
</html>