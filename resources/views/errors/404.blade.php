@extends('template.master')
@section('region_editable')
    <div class="jumbotron">
        <h1>Oops!!</h1>
        <p>La página que usted esta buscando ya no se encuentra aquí o simplemente usted esta buscando algo que nunca existió.</p>
        <p><a href="{{ route('home') }}" class="btn btn-primary btn-lg">Volver al inicio</a></p>
    </div>
@stop