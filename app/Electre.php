<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Electre extends Model
{
    protected $table = "electres";
    protected $fillable = ['titulo','criterio','alternativa','alfa','beta','user_id'];

    public function users(){
        return $this->belongsTo('App\User','user_id');
    }
}
