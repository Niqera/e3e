<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestController extends Controller
{

    public function testeo(){
        $criterio = 5;
        $altenativa = 5;
        $indiferencia = [25,16,1,12,10];
        $preferencia = [50,24,2,24,20];
        $veto = [100,60,3,48,90];
        $peso = [1,1,1,1,1];
        $desempeño = [[-14,90,0,40,100],[129,100,0,0,0],[-10,50,0,10,100],[44,90,0,5,20],[-14,100,0,20,40]];
        $alfa = -0.15;
        $beta = 0.30;

        $this->electre(2,$criterio,$altenativa,$desempeño,$peso,$indiferencia,$preferencia,$veto,$alfa,$beta);
    }
    /*
     * $id = Id del proyecto;
     * $n = Número de Criterios;
     * $m = Número de Alternativas;
     */
    public function electre($id,$n,$m,$desempeño,$peso,$indiferencia,$preferencia,$veto,$alfa,$beta){
        $raiz = "repositorio/".$id;
        $dir = $raiz."/".$id;

        /*Creo la matriz de desempeño*/
        $matriz_desempeño = array();
        $aux_matriz_desempeño = array();

        $aux_criterio = json_decode(file_get_contents($dir.'_criterio.json'),true);

        for($i = 0; $i < $m; $i++){
            for($j = 0; $j < $n; $j++){
                if($aux_criterio['C'.$j]['tipo'] == 'default'){
                    $matriz_desempeño[$i][$j] = $desempeño[$i][$j];
                }else{
                    $matriz_desempeño[$i][$j] = 1/$desempeño[$i][$j];
                }
                $aux_matriz_desempeño[$i][$j] = $desempeño[$i][$j];
            }
        }

        $concordancia = $this->matriz_concordancia($m,$n,$matriz_desempeño,$peso,$preferencia,$indiferencia);

        $discordancia = $this->matriz_discordancia($m,$n,$matriz_desempeño,$preferencia,$veto);

        $credibilidad = $this->matriz_credibilidad($m,$concordancia,$discordancia);

        $proyectos = array();
        for($i = 0; $i < $m; $i++){
            $proyectos[$i] = $i;
        }

        $destilacion = $this->primera_destilacion($proyectos,$credibilidad,$alfa,$beta);

        $primera_destilacion = $this->ordena_primera_destilacion($destilacion);

        $ranking = array();
        $puntero = 0;

        while(count($destilacion) > 1){

            $max = max($destilacion);

            $temporal = array();
            $cont = 0;

            for ($i = 0; $i < count($destilacion); $i++) {
                if ($max[0] == $destilacion[$i][0]) {
                    $temporal[$cont] = $destilacion[$i];
                    unset($destilacion[$i]);
                    $cont++;
                }
            }

            $ranking[$puntero] = $temporal;

            $destilacion = array_values($destilacion);

            $proyectos = array();
            for($i = 0; $i < count($destilacion); $i++){
                $proyectos[$i] = $destilacion[$i]['proyecto'];
            }

            $destilacion = $this->destilacion($proyectos, $credibilidad, $alfa, $beta);

            $puntero++;

        }

        $ranking[$puntero] = $destilacion;

        $aux = count($ranking);
        $puntero = 0;
        while($aux > 0){
            if(count($ranking[$puntero]) > 1){
                $proyectos = array();
                for($i = 0; $i < count($ranking[$puntero]); $i++){
                    $proyectos[$i] = $ranking[$puntero][$i]['proyecto'];
                }
                $ranking[$puntero] = $this->destilacion($proyectos, $credibilidad, $alfa,$beta);
                $puntero++;
            }
            $aux--;
        }

        $data = [$primera_destilacion,$ranking];

        return $data;

    }

    public function matriz_concordancia($m,$n,$matriz_desempeño,$peso,$preferencia,$indiferencia){
        /*Genero la suma de los pesos*/
        $suma_pesos = 0;

        for ($i=0; $i < count($peso) ; $i++) {
            $suma_pesos = $suma_pesos + $peso[$i];
        }

        /*Genero matriz de concordancia*/
        $concordancia = array();

        for ($i=0; $i < $m; $i++) { //selecciona la alternativa a comparar
            for ($j=0; $j < $m; $j++) { //selecciona las alternativas que seran comparadas
                if ( $i == $j ) {//si las alternativas son iguales en ese punto el elemento de la concordancia sera 1
                    $concordancia[$i][$j] = "-";
                }else{
                    $aux = [];
                    for ($k=0; $k < $n; $k++) { //revolver de criterios
                        if ($matriz_desempeño[$i][$k] + $preferencia[$k] <= $matriz_desempeño[$j][$k]) {
                            $aux[$k] = 0.00;
                        } else {
                            if ($matriz_desempeño[$i][$k]+$indiferencia[$k] >= $matriz_desempeño[$j][$k]) {
                                $aux[$k] = 1.00;
                            } else {
                                $aux[$k] = ($preferencia[$k] + $matriz_desempeño[$i][$k] - $matriz_desempeño[$j][$k])/($preferencia[$k] - $indiferencia[$k]);
                            }
                        }
                    }

                    $numerador = 0;
                    for ($l=0; $l < count($aux); $l++) {
                        $numerador = $numerador + ($peso[$l] * $aux[$l]);
                    }

                    $concordancia[$i][$j] = $numerador/$suma_pesos;
                }
            }
        }

        return $concordancia;
    }

    public function matriz_discordancia($m,$n,$matriz_desempeño,$preferencia,$veto){
        $discordancia = array();
        if (count($veto) == $n) {
            for ($i=0; $i < $m; $i++) { //selecciona la alternativa a comparar
                for ($j=0; $j < $m; $j++) { //selecciona las alternativas que seran comparadas
                    if ( $i == $j ) {//si las alternativas son iguales en ese punto el elemento de la concordancia sera 1
                        $discordancia[$i][$j] = "-";
                    }else{
                        $aux_dis = [];
                        for ($k=0; $k < $n; $k++) { //revolver de criterios
                            if ($matriz_desempeño[$i][$k] + $preferencia[$k] >= $matriz_desempeño[$j][$k]) {
                                $aux_dis[$k] = 0.00;
                            } else {
                                if ($matriz_desempeño[$i][$k]+$veto[$k] <= $matriz_desempeño[$j][$k]) {
                                    $aux_dis[$k] = 1.00;
                                } else {
                                    $aux_dis[$k] = ($matriz_desempeño[$j][$k] - $matriz_desempeño[$i][$k] - $preferencia[$k])/($veto[$k] - $preferencia[$k]);
                                }
                            }
                        }
                        $discordancia[$i][$j] = $aux_dis;
                    }
                }
            }
        } else {
            for ($i=0; $i < $m; $i++) {
                for ($j=0; $j < $m; $j++) {
                    $discordancia[$i][$j] = 0;
                }
            }
        }

        return $discordancia;
    }

    public function matriz_credibilidad($m,$concordancia,$discordancia){
        $credibilidad = array();
        for ($i=0; $i < $m; $i++) { //selecciona la alternativa a comparar
            for ($j=0; $j < $m; $j++) { //selecciona las alternativas que seran comparadas
                if ( $i == $j ) {//si las alternativas son iguales en ese punto el elemento de la concordancia sera 1
                    $credibilidad[$i][$j] = "-";
                }else{
                    $elem_discordancia = $discordancia[$i][$j];
                    $band = true;
                    for ($k=0; $k < count($elem_discordancia); $k++) { //revolver de criterios
                        if ($elem_discordancia[$k] <= $concordancia[$i][$j]) {
                            $band = true;
                        } else {
                            $band = false;
                            $k = count($elem_discordancia);
                        }
                    }

                    if ($band) {
                        $credibilidad[$i][$j] = $concordancia[$i][$j];
                    } else {
                        $acumulador = 1;
                        for ($k=0; $k < count($elem_discordancia); $k++) {
                            if ($elem_discordancia[$k] > $concordancia[$i][$j]) {
                                $acumulador = $acumulador * ((1-$elem_discordancia[$k])/(1-$concordancia[$i][$j]));
                            }
                        }
                        $credibilidad[$i][$j] = $concordancia[$i][$j]*$acumulador;
                    }
                }
            }
        }

        return $credibilidad;
    }

    public function primera_destilacion($proyectos,$credibilidad,$alfa,$beta)
    {
        $auxiliar = array();
        for ($i = 0; $i < count($credibilidad); $i++) {
            $auxiliar[$i] = max($credibilidad[$i]);
        }
        $lambda_max = max($auxiliar);

        $calificaciones_netas = $this->matrices_lambda($proyectos,$lambda_max,$credibilidad,$alfa,$beta);

        return $calificaciones_netas;
    }

    public function destilacion($proyectos,$credibilidad,$alfa,$beta){

        $nueva_credibilidad = array();

        for($i = 0; $i < count($proyectos); $i++){
            for($j = 0; $j < count($proyectos); $j++){
                if($i == $j){
                    $nueva_credibilidad[$i][$j] = "-";
                }else{
                    $nueva_credibilidad[$i][$j] = $credibilidad[$proyectos[$i]][$proyectos[$j]];
                }
            }
        }

        $auxiliar = array();
        for($i = 0; $i < count($nueva_credibilidad); $i++){
            $auxiliar[$i] = max($nueva_credibilidad[$i]);
        }

        $lambda_max = max($auxiliar);

        $calificaciones_netas = $this->matrices_lambda($proyectos,$lambda_max,$nueva_credibilidad,$alfa,$beta);

        return $calificaciones_netas;

    }

    public function matrices_lambda($proyectos,$lambda_max,$credibilidad,$alfa,$beta){

        $lambda = $lambda_max - ($beta + ($alfa * $lambda_max));

        $lambda_strength = array();
        $lambda_weakness = array();

        for ($i = 0; $i < count($proyectos); $i++) {
            for ($j = 0; $j < count($proyectos); $j++) {
                if ($i == $j) {
                    $lambda_strength[$i][$j] = "-";
                    $lambda_weakness[$i][$j] = "-";
                } else {
                    if ($credibilidad[$i][$j] > $lambda) {
                        $lambda_strength[$i][$j] = 1;
                    } else {
                        $lambda_strength[$i][$j] = 0;
                    }

                    if ((1 - ($beta + ($alfa * $lambda))) * $credibilidad[$i][$j] > $credibilidad[$j][$i]) {
                        $lambda_weakness[$i][$j] = 1;
                    } else {
                        $lambda_weakness[$i][$j] = 0;
                    }
                }
            }
        }

        $calificaciones_positivas = array();

        for ($i = 0; $i < count($proyectos); $i++) {
            $acumulador = 0;
            for ($j = 0; $j < count($proyectos); $j++) {
                if ($i != $j) {
                    $acumulador = $acumulador + $lambda_strength[$i][$j];
                }
            }
            $calificaciones_positivas[$i] = $acumulador;
        }

        $calificaciones_negativas = array();
        for ($i = 0; $i < count($proyectos); $i++) {
            $acumulador = 0;
            for ($j = 0; $j < count($proyectos); $j++) {
                if ($i != $j) {
                    $acumulador = $acumulador + $lambda_weakness[$j][$i];
                }
            }
            $calificaciones_negativas[$i] = $acumulador;
        }

        $calificaciones_netas = array();
        for ($i = 0; $i < count($proyectos); $i++) {
            $calificaciones_netas[$i] = [$calificaciones_positivas[$i] - $calificaciones_negativas[$i],'proyecto'=>$proyectos[$i]];
        }

        return $calificaciones_netas;
    }

    /*
     * Solo es aplicable para ordenar la primera destilacion
     * */
    public function ordena_primera_destilacion($destilacion){
        $temp = array();
        for($i = 1; $i < count($destilacion); $i++){
            for($j = 0; $j < $i; $j++){
                if($destilacion[$i][0] > $destilacion[$j][0]){
                    $temp[0] = $destilacion[$j];
                    $destilacion[$j] = $destilacion[$i];
                    $destilacion[$i] = $temp[0];
                }
            }
        }

        return $destilacion;
    }
}
