<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        /*Auth::login($this->create($request->all()));

        return redirect($this->redirectPath());*/

        $data = ['name'=>$request->name,'email'=>$request->email,'token'=>bcrypt($request->email)];
        Mail::send('email.verification',['data'=>$data],function($m) use ($data){
            $m->to($data['email'], $data['name'])->subject('Electre III - Verificación de Email');
        });

        $this->create($request->all());

        return redirect()->route('authLogin')->with('alert-success','Se envió un email de verificación de la cuenta, ingrese a su correo y confirme para poder activar su cuenta. Gracias!!!');
    }

    public function getConfirmation($email){
        if($user = User::where('email','=',$email)->first()){
            $user->status = "on";
            $user->save();
            return redirect()->route('authLogin')->with('alert-success','Se ha verificado con éxito, ahora ya puede iniciar sesión. Gracias!!!');
        }else{
            return redirect()->route('authRegister')->with('alert-danger','Lo sentimos pero usted no esta registrado.');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role' => 'user',
            'status' => 'off'
        ]);
    }
}
