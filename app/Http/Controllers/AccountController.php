<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.edit');
    }

    public function editAccount(Request $request){
        $this->validate($request,[
            'name' => 'required|max:255',
            'password' => 'confirmed|min:6',
        ]);

        $user = User::find(Auth::user()->id);
        $user->name = $request->name;

        if(empty($request->password)){
            $user->save();
            return redirect()->route('home')->with('alert-success','Se actualizaron sus datos con éxito.');
        }

        $user->password = bcrypt($request->password);
        $user->save();
        return redirect()->route('home')->with('alert-success','Se actualizaron sus datos con éxito.');

    }

    public function listAccount(){
        if(Auth::user()->role == "admin"){
            $users = User::orderBy('id','desc')->paginate(20);
            return view('usuariosListado',compact('users'));
        }

        return redirect()->route('home');
    }

    public function editAccountUser(Request $request,$id){
        if(Auth::user()->role == "admin"){
            if($user = User::find($id)){
                $user->role = $request->role;
                $user->save();
                return redirect()->route('listAccount')->with('alert-success','Se cambio de rol con éxito.');
            }

            return redirect()->route('home');
        }

        return redirect()->route('home');
    }

    public function deleteAccountUser($id){
        if(Auth::user()->role == "admin"){
            if($user = User::find($id)){
                $user->delete();
                return redirect()->route('listAccount')->with('alert-success','El usuario fue eliminado con éxito.');
            }else{
                return redirect()->route('home');
            }
        }
        return redirect()->route('home');
    }
}
