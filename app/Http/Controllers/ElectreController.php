<?php

namespace App\Http\Controllers;

use App\Electre;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class ElectreController extends Controller
{
    /*PROYECTOS*/

    function listarProyecto(){
        $proyectos = Electre::orderBy('id','desc')->paginate(20);
        $titulo = "Listado de Proyectos";
        return view('proyectoListar',compact('proyectos','titulo'));
    }

    function listarMisProyectos(){
        $proyectos = Electre::where('user_id',Auth::user()->id)->orderBy('id','desc')->paginate(20);
        $titulo = "Mis Proyectos";
        return view('proyectoListar',compact('proyectos','titulo'));
    }

    function crearProyecto(){
        return view('proyectoCrear');
    }

    function almacenarProyecto(Request $request){
        $this->validate($request,[
            'Titulo' => 'required',
            'Criterio' => 'required|numeric|min:2',
            'Alternativa' => 'required|numeric|min:2',
            'Alfa' => 'required|numeric',
            'Beta' => 'required|numeric'
        ]);

        $inserted = Electre::create([
            'titulo' => $request->Titulo,
            'criterio' => $request->Criterio,
            'alternativa' => $request->Alternativa,
            'alfa' => $request->Alfa,
            'beta' => $request->Beta,
            'user_id' => Auth::user()->id
        ]);

        if($inserted){
            return redirect()->route('cargarElemento',$inserted->id)->with('alert-success','El proyecto se ha creado con éxito, ahora deberá declarar los títulos de los criterios y alternativas.');
        }else{
            return redirect()->route('home')->with('alert-danger','Oops! Ocurrió un problema al guardar el proyecto, por favor comunicate con el administrador para solucionar este problema, escribir al email gianfranconh@gmail.com.');
        }
    }

    function actualizarProyecto(Request $request,$id){
        $this->validate($request,[
            'Titulo' => 'required',
            'Alfa' => 'required|numeric',
            'Beta' => 'required|numeric'
        ]);

        if($proyecto = Electre::find($id)){
            if($proyecto->user_id == Auth::user()->id || Auth::user()->role == "admin"){
                $proyecto->titulo = $request->Titulo;
                $proyecto->alfa = $request->Alfa;
                $proyecto->beta = $request->Beta;
                $proyecto->save();

                return redirect()->route('editarProyecto',$id)->with('alert-success','Se actualizó el título del proyecto con éxito.');
            }else{
                return redirect()->route('home')->with('alert-danger','Usted esta intentando vulnerar el sistema, el administrador del servidor será notificado de sus movimientos.');
            }
        }else{
            return redirect()->route('home')->with('Oops!!! Error al intentar actualizar un proyecto que no existe.');
        }
    }

    function editarProyecto($id){
        if($proyecto = Electre::find($id)){

            if($proyecto->user_id != Auth::user()->id && Auth::user()->role == "user"){
                return redirect()->route('home')->with('alert-danger','Usted esta intentando vulnerar el sistema, el administrador del servidor será notificado de sus movimientos.');
            }

            $raiz = "repositorio/".$id;
            $dir = $raiz."/".$id;

            if(is_file($dir.'_criterio.json') && is_file($dir.'_alternativa.json')){
                if(
                    is_file($dir.'_aux_desempeno.json') &&
                    is_file($dir.'_peso.json') &&
                    is_file($dir.'_indiferencia.json') &&
                    is_file($dir.'_preferencia.json') &&
                    is_file($dir.'_veto.json')
                ){

                    $criterios = json_decode(file_get_contents($dir.'_criterio.json'),true);
                    $alternativas = json_decode(file_get_contents($dir.'_alternativa.json'),true);
                    $desempeno = json_decode(file_get_contents($dir.'_aux_desempeno.json'),true);
                    $peso = json_decode(file_get_contents($dir.'_peso.json'),true);
                    $indiferencia = json_decode(file_get_contents($dir.'_indiferencia.json'),true);
                    $preferencia = json_decode(file_get_contents($dir.'_preferencia.json'),true);
                    $veto = json_decode(file_get_contents($dir.'_veto.json'),true);

                    return view('proyectoEditar',compact('proyecto','criterios','alternativas','desempeno','peso','indiferencia','preferencia','veto'));
                }else{
                    return redirect()->route('cargarDatos',$id)->with('alert-warning','Este proyecto aun no tiene los datos necesarios para la matriz de desempeño, ingrese los datos solicitados.');
                }
            }else{
                return redirect()->route('cargarElemento',$id)->with('alert-warning','Este proyecto aun no tiene datos principales, por favor ingrese los titulos de los criterios y alternativas.');
            }
        }else{
            return redirect()->route('home')->with('alert-danger','El proyecto no exite, por favor ingrese a un proyecto valido.');
        }
    }

    function mostrarProyecto($id){
        if($proyecto = Electre::find($id)){

            $raiz = "repositorio/".$id;
            $dir = $raiz."/".$id;

            if(is_dir($raiz)){
                if(
                    is_file($dir.'_criterio.json') &&
                    is_file($dir.'_alternativa.json')
                ){
                    if(
                        is_file($dir.'_aux_desempeno.json') &&
                        is_file($dir.'_peso.json') &&
                        is_file($dir.'_indiferencia.json') &&
                        is_file($dir.'_preferencia.json') &&
                        is_file($dir.'_veto.json')
                    ){

                        $criterios = json_decode(file_get_contents($dir.'_criterio.json'),true);
                        $alternativas = json_decode(file_get_contents($dir.'_alternativa.json'),true);
                        $desempeno = json_decode(file_get_contents($dir.'_aux_desempeno.json'),true);
                        $peso = json_decode(file_get_contents($dir.'_peso.json'),true);
                        $indiferencia = json_decode(file_get_contents($dir.'_indiferencia.json'),true);
                        $preferencia = json_decode(file_get_contents($dir.'_preferencia.json'),true);
                        $veto = json_decode(file_get_contents($dir.'_veto.json'),true);

                        $concordancia = json_decode(file_get_contents($dir.'_concordancia.json'),true);
                        $discordancia = json_decode(file_get_contents($dir.'_discordancia.json'),true);
                        $credibilidad = json_decode(file_get_contents($dir.'_credibilidad.json'),true);

                        $primer_destilado = json_decode(file_get_contents($dir.'_primera_destilacion.json'),true);
                        $ranking_descendente = json_decode(file_get_contents($dir.'_ranking_descendente.json'),true);
                        $ranking_ascendente = json_decode(file_get_contents($dir.'_ranking_ascendente.json'),true);

                        return view(
                            'proyectoMostrar',
                            compact(
                                'proyecto',
                                'criterios',
                                'alternativas',
                                'desempeno',
                                'peso',
                                'indiferencia',
                                'preferencia',
                                'veto',
                                'concordancia',
                                'discordancia',
                                'credibilidad',
                                'primer_destilado',
                                'ranking_descendente',
                                'ranking_ascendente')
                        );

                    }else{
                        return redirect()->route('cargarDatos',$id)->with('alert-warning','Este proyecto aun no tiene los datos necesarios para la matriz de desempeño, ingrese los datos solicitados.');
                    }
                }else{
                    return redirect()->route('cargarElemento',$id)->with('alert-warning','Este proyecto aun no tiene datos principales, por favor ingrese los titulos de los criterios y alternativas.');
                }
            }else{
                return redirect()->route('cargarElemento',$id)->with('alert-warning','Este proyecto aun no tiene datos principales, por favor ingrese los titulos de los criterios y alternativas.');
            }
        }else{
            return redirect()->route('home')->with('alert-danger','Oops! Los resultados del proyecto que usted esta buscando aun no existe o fue eliminado.');
        }
    }

    function eliminarProyecto(Request $request,$id){
        if($proyecto = Electre::find($id)){

            if($proyecto->user_id != Auth::user()->id && Auth::user()->role == "user"){
                return redirect()->route('home')->with('alert-danger','Usted esta intentando vulnerar el sistema, el administrador del servidor será notificado de sus movimientos.');
            }

            $raiz = "repositorio/".$id;
            $dir = $raiz."/".$id;

            if(is_dir($raiz)){
                $files = [
                    '_criterio.json',
                    '_alternativa.json',
                    '_aux_desempeno.json',
                    '_desempeno.json',
                    '_peso.json',
                    '_indiferencia.json',
                    '_preferencia.json',
                    '_veto.json',
                    '_concordancia.json',
                    '_discordancia.json',
                    '_credibilidad.json',
                    '_primera_destilacion.json',
                    '_ranking_descendente.json',
                    '_ranking_ascendente.json'];

                for($i = 0; $i < count($files); $i++){
                    if(is_file($dir.$files[$i])){
                        unlink($dir.$files[$i]);
                    }
                }

                rmdir($raiz);
            }

            $proyecto->delete();

            return redirect()->route('home')->with('alert-success','Se eliminó el proyecto con éxito');
        }else{
            return redirect()->route('home')->with('alert-danger','Se presento un problema al intentar eliminar el proyecto');
        }
    }

    /*ELEMENTOS*/

    function crearElemento($id){
        if($proyecto = Electre::find($id)){

            if($proyecto->user_id != Auth::user()->id && Auth::user()->role == "user"){
                return redirect()->route('home')->with('alert-danger','Usted esta intentando vulnerar el sistema, el administrador del servidor será notificado de sus movimientos.');
            }

            return view('elementoCargar',compact('proyecto'));
        }else{
            return redirect()->route('home')->with('alert-danger','Ingreso incorrecto, el proyecto no existe.');
        }
    }

    function almacenarElemento(Request $request){

        $this->validate($request,[
            'id_proyecto' => 'required|numeric',
            'criterio' => 'required|array',
            'alternativa' => 'required|array',
            'tipo' => 'in:default,inverso|array'
        ]);

        if($proyecto = Electre::find($request->id_proyecto)){

            if($proyecto->user_id != Auth::user()->id  && Auth::user()->role == "user"){
                return redirect()->route('home')->with('alert-danger','Usted esta intentando vulnerar el sistema, el administrador del servidor será notificado de sus movimientos.');
            }

            $raiz = "repositorio/".$request->id_proyecto;
            $dir = $raiz."/".$request->id_proyecto;

            if(!is_dir($raiz)){
                mkdir($raiz,0777,true);
            }

            $aux_criterio = $request->criterio;
            $aux_tipo = $request->tipo;
            $aux_alternativa = $request->alternativa;

            $criterio = array();
            for($i = 0; $i < $proyecto->criterio; $i++){
                $criterio['C'.$i]['etiqueta'] = $aux_criterio[$i];
                $criterio['C'.$i]['tipo'] = $aux_tipo[$i];
            }

            $alternativa = array();
            for($i = 0; $i < $proyecto->alternativa; $i++){
                $alternativa['A'.$i]['etiqueta'] = $aux_alternativa[$i];
            }

            file_put_contents($dir."_criterio.json",json_encode($criterio));
            file_put_contents($dir."_alternativa.json",json_encode($alternativa));

            return redirect()->route('cargarDatos',$request->id_proyecto)->with('alert-success','Se han creado las etiquetas con éxito, ahora debe cargar la matriz de desempeño, pesos y umbrales.');
        }else{
            return redirect()->route('home')->with('alert-danger','Oops!!! Surgió un error al intentar almacenar los títulos de los criterios y alternativas.');
        }
    }

    function actualizarElemento(Request $request){

        $this->validate($request,[
            'id_proyecto' => 'required|numeric',
            'criterio' => 'required|array',
            'alternativa' => 'required|array',
            'tipo' => 'in:default,inverso|array'
        ]);

        if($proyecto = Electre::find($request->id_proyecto)){

            if($proyecto->user_id != Auth::user()->id && Auth::user()->role == "user"){
                return redirect()->route('home')->with('alert-danger','Usted esta intentando vulnerar el sistema, el administrador del servidor será notificado de sus movimientos.');
            }

            $raiz = "repositorio/".$request->id_proyecto;
            $dir = $raiz."/".$request->id_proyecto;

            if(!is_dir($raiz)){
                mkdir($raiz,0777,true);
            }

            $aux_criterio = $request->criterio;
            $aux_tipo = $request->tipo;
            $aux_alternativa = $request->alternativa;

            $criterio = array();
            for($i = 0; $i < $proyecto->criterio; $i++){
                $criterio['C'.$i]['etiqueta'] = $aux_criterio[$i];
                $criterio['C'.$i]['tipo'] = $aux_tipo[$i];
            }

            $alternativa = array();
            for($i = 0; $i < $proyecto->alternativa; $i++){
                $alternativa['A'.$i]['etiqueta'] = $aux_alternativa[$i];
            }

            $copia_criterio = json_decode(file_get_contents($dir.$request->id_proyecto.'_criterio.json'),true);

            file_put_contents($dir."_criterio.json",json_encode($criterio));
            file_put_contents($dir."_alternativa.json",json_encode($alternativa));


            /*Paso 03*/
            $indiferencia = json_decode(file_get_contents($dir.'_indiferencia.json'),true);
            $preferencia = json_decode(file_get_contents($dir.'_preferencia.json'),true);
            $veto = json_decode(file_get_contents($dir.'_veto.json'),true);

            /*Paso 04*/
            $peso = json_decode(file_get_contents($dir.'_peso.json'),true);

            /*Paso 05*/
            $desempeño = array();
            $aux_desempeño = array();

            $json_desempeño = json_decode(file_get_contents($dir.'_aux_desempeno.json'),true);

            $aux_criterio = json_decode(file_get_contents($dir.'_criterio.json'),true);

            for($i = 0; $i < $proyecto->alternativa; $i++){
                for($j = 0; $j < $proyecto->criterio; $j++){
                    if($aux_criterio['C'.$j]['tipo'] == 'default'){
                        $desempeño[$i][$j] = $json_desempeño[$i][$j];
                    }else{
                        if($json_desempeño[$i][$j] == 0){
                            file_put_contents($dir."_criterio.json",json_encode($copia_criterio));
                            return redirect()->route('editarProyecto',$request->id_proyecto)->with('alert-warning','El elemento A'.($i+1).'C'.($j+1).' de la matriz de desempeño debe ser mayor a 0');
                        }else{
                            $desempeño[$i][$j] = 1/$json_desempeño[$i][$j];
                        }
                    }
                    $aux_desempeño[$i][$j] = $json_desempeño[$i][$j];
                }
            }

            $this->electre(
                $proyecto->id,
                $proyecto->criterio,
                $proyecto->alternativa,
                $desempeño,
                $peso,
                $indiferencia,
                $preferencia,
                $veto,
                $proyecto->alfa,
                $proyecto->beta
            );

            return redirect()->route('editarProyecto',$request->id_proyecto)->with('alert-success','Se han actualizado las etiquetas con éxito.');
        }else{
            return redirect()->route('home')->with('alert-danger','Oops!!! Surgió un error al intentar almacenar los títulos de los criterios y alternativas.');
        }
    }

    /*DATOS*/

    function cargarDatos($id){
        if($proyecto = Electre::find($id)){
            if($proyecto->user_id != Auth::user()->id  && Auth::user()->role == "user"){
                return redirect()->route('home')->with('alert-danger','Usted esta intentando vulnerar el sistema, el administrador del servidor será notificado de sus movimientos.');
            }

            $raiz = "repositorio/".$id;
            $dir = $raiz."/".$id;

            if(!is_dir($raiz)){
                mkdir($raiz,0777,true);
            }

            $criterios = json_decode(file_get_contents($dir.'_criterio.json'),true);
            $alternativas = json_decode(file_get_contents($dir.'_alternativa.json'),true);

            return view('datosCargar',compact('proyecto','criterios','alternativas'));
        }else{
            return redirect()->route('home');
        }
    }

    function almacenarDatos(Request $request){
        if($proyecto = Electre::find($request->id_proyecto)){

            $this->validate($request,[
                'desempeno' =>  'required|array',
                'peso'      =>  'required|array',
                'indiferencia'  => 'required|array',
                'preferencia'  => 'required|array',
                'veto'  => 'required|array'
            ]);

            if($proyecto->user_id != Auth::user()->id && Auth::user()->role == "user"){
                return redirect()->route('home')->with('alert-danger','Usted esta intentando vulnerar el sistema, el administrador del servidor será notificado de sus movimientos.');
            }

            $this->electre(
                $proyecto->id,
                $proyecto->criterio,
                $proyecto->alternativa,
                $request->desempeno,
                $request->peso,
                $request->indiferencia,
                $request->preferencia,
                $request->veto,
                $proyecto->alfa,
                $proyecto->beta
            );

            return redirect()->route('mostrarProyecto',$request->id_proyecto);

        }else{
            return redirect()->route('home')->with('alert-danger','Oops! EL proyecto que intentas encontrar no existe, posiblemente aun no se crea o ya fue eliminado.');
        }
    }

    function actualizarDatos(Request $request){
        if($proyecto = Electre::find($request->id_proyecto)){
            $this->validate($request,[
                'desempeno' =>  'required|array',
                'peso'      =>  'required|array',
                'indiferencia'  => 'required|array',
                'preferencia'  => 'required|array',
                'veto'  => 'required|array'
            ]);

            if($proyecto->user_id != Auth::user()->id && Auth::user()->role == "user"){
                return redirect()->route('home')->with('alert-danger','Usted esta intentando vulnerar el sistema, el administrador del servidor será notificado de sus movimientos.');
            }

            $this->electre(
                $proyecto->id,
                $proyecto->criterio,
                $proyecto->alternativa,
                $request->desempeno,
                $request->peso,
                $request->indiferencia,
                $request->preferencia,
                $request->veto,
                $proyecto->alfa,
                $proyecto->beta
            );

            return redirect()->route('editarProyecto',$request->id_proyecto)->with('alert-success','La matriz de desempeño se actualizó con éxito.');

        }else{
            return redirect()->route('home')->with('alert-danger','Oops! EL proyecto que intentas encontrar no existe, posiblemente aun no se crea o ya fue eliminado.');
        }
    }

    /*ALGORITMO ELECTRE III*/

    public function electre($id,$n,$m,$desempeño,$peso,$indiferencia,$preferencia,$veto,$alfa,$beta){
        $raiz = "repositorio/".$id;
        $dir = $raiz."/".$id;

        /*Creo la matriz de desempeño*/
        $matriz_desempeño = array();
        $aux_matriz_desempeño = array();

        $aux_criterio = json_decode(file_get_contents($dir.'_criterio.json'),true);

        for($i = 0; $i < $m; $i++){
            for($j = 0; $j < $n; $j++){
                if($aux_criterio['C'.$j]['tipo'] == 'default'){
                    $matriz_desempeño[$i][$j] = $desempeño[$i][$j];
                }else{
                    $matriz_desempeño[$i][$j] = 1/$desempeño[$i][$j];
                }
                $aux_matriz_desempeño[$i][$j] = $desempeño[$i][$j];
            }
        }

        $concordancia = $this->matriz_concordancia($m,$n,$matriz_desempeño,$peso,$preferencia,$indiferencia);

        $discordancia = $this->matriz_discordancia($m,$n,$matriz_desempeño,$preferencia,$veto);

        $credibilidad = $this->matriz_credibilidad($m,$concordancia,$discordancia);

        $proyectos = array();
        for($i = 0; $i < $m; $i++){
            $proyectos[$i] = $i;
        }

        $destilacion = $this->primera_destilacion($proyectos,$credibilidad,$alfa,$beta);

        $destilacion_desc = $destilacion;
        $destilacion_asc = $destilacion;

        $primera_destilacion = $this->ordena_primera_destilacion($destilacion);

        /*DESTILACION DESCENDENTE*/
        $ranking_descendente = array();
        $puntero = 0;

        while(count($destilacion_desc) > 1){

            $max = max($destilacion_desc);

            $temporal = array();
            $cont = 0;

            for ($i = 0; $i < count($destilacion_desc); $i++) {
                if ($max[0] == $destilacion_desc[$i][0]) {
                    $temporal[$cont] = $destilacion_desc[$i];
                    unset($destilacion_desc[$i]);
                    $cont++;
                }
            }

            $ranking_descendente[$puntero] = $temporal;

            $destilacion_desc = array_values($destilacion_desc);

            $proyectos = array();
            for($i = 0; $i < count($destilacion_desc); $i++){
                $proyectos[$i] = $destilacion_desc[$i]['proyecto'];
            }

            $destilacion_desc = $this->destilacion($proyectos, $credibilidad, $alfa, $beta);

            $puntero++;

        }

        $ranking_descendente[$puntero] = $destilacion_desc;

        $aux = count($ranking_descendente);
        $puntero = 0;
        while($aux > 0){
            if(count($ranking_descendente[$puntero]) > 1){
                $proyectos = array();
                for($i = 0; $i < count($ranking_descendente[$puntero]); $i++){
                    $proyectos[$i] = $ranking_descendente[$puntero][$i]['proyecto'];
                }
                $ranking_descendente[$puntero] = $this->destilacion($proyectos, $credibilidad, $alfa,$beta);
                $puntero++;
            }
            $aux--;
        }
        /*FIN DESTILACION DESCENDENTE*/

        /*DESTILACION ASCENDENTE*/
        $ranking_ascendente = array();
        $puntero = 0;

        while(count($destilacion_asc) > 1){

            $min = min($destilacion_asc);

            $temporal = array();
            $cont = 0;

            for ($i = 0; $i < count($destilacion_asc); $i++) {
                if ($min[0] == $destilacion_asc[$i][0]) {
                    $temporal[$cont] = $destilacion_asc[$i];
                    unset($destilacion_asc[$i]);
                    $cont++;
                }
            }

            $ranking_ascendente[$puntero] = $temporal;

            $destilacion_asc = array_values($destilacion_asc);

            $proyectos = array();
            for($i = 0; $i < count($destilacion_asc); $i++){
                $proyectos[$i] = $destilacion_asc[$i]['proyecto'];
            }

            $destilacion_asc = $this->destilacion($proyectos, $credibilidad, $alfa, $beta);

            $puntero++;

        }

        $ranking_ascendente[$puntero] = $destilacion_asc;

        $aux = count($ranking_ascendente);
        $puntero = 0;
        while($aux > 0){
            if(count($ranking_ascendente[$puntero]) > 1){
                $proyectos = array();
                for($i = 0; $i < count($ranking_ascendente[$puntero]); $i++){
                    $proyectos[$i] = $ranking_ascendente[$puntero][$i]['proyecto'];
                }
                $ranking_ascendente[$puntero] = $this->destilacion($proyectos, $credibilidad, $alfa,$beta);
                $puntero++;
            }
            $aux--;
        }
        /*FIN DESTILACION ASCENDENTE*/

        /*RESULTADOS*/
        $data = [$primera_destilacion,$ranking_descendente,$ranking_ascendente];

        if(!is_dir($raiz)){
            mkdir($raiz,0777,true);
        }

        file_put_contents($dir."_aux_desempeno.json",json_encode($aux_matriz_desempeño));
        file_put_contents($dir."_desempeno.json",json_encode($matriz_desempeño));

        file_put_contents($dir."_peso.json",json_encode($peso));
        file_put_contents($dir."_indiferencia.json",json_encode($indiferencia));
        file_put_contents($dir."_preferencia.json",json_encode($preferencia));
        file_put_contents($dir."_veto.json",json_encode($veto));

        file_put_contents($dir."_concordancia.json",json_encode($concordancia));
        file_put_contents($dir."_discordancia.json",json_encode($discordancia));
        file_put_contents($dir."_credibilidad.json",json_encode($credibilidad));

        file_put_contents($dir."_primera_destilacion.json",json_encode($primera_destilacion));
        file_put_contents($dir."_ranking_descendente.json",json_encode($ranking_descendente));
        file_put_contents($dir."_ranking_ascendente.json",json_encode($ranking_ascendente));

        return $data;
    }

    public function matriz_concordancia($m,$n,$matriz_desempeño,$peso,$preferencia,$indiferencia){
        /*Genero la suma de los pesos*/
        $suma_pesos = 0;

        for ($i=0; $i < count($peso) ; $i++) {
            $suma_pesos = $suma_pesos + $peso[$i];
        }

        /*Genero matriz de concordancia*/
        $concordancia = array();

        for ($i=0; $i < $m; $i++) { //selecciona la alternativa a comparar
            for ($j=0; $j < $m; $j++) { //selecciona las alternativas que seran comparadas
                if ( $i == $j ) {//si las alternativas son iguales en ese punto el elemento de la concordancia sera 1
                    $concordancia[$i][$j] = "-";
                }else{
                    $aux = [];
                    for ($k=0; $k < $n; $k++) { //revolver de criterios
                        if ($matriz_desempeño[$i][$k] + $preferencia[$k] <= $matriz_desempeño[$j][$k]) {
                            $aux[$k] = 0.00;
                        } else {
                            if ($matriz_desempeño[$i][$k]+$indiferencia[$k] >= $matriz_desempeño[$j][$k]) {
                                $aux[$k] = 1.00;
                            } else {
                                $aux[$k] = ($preferencia[$k] + $matriz_desempeño[$i][$k] - $matriz_desempeño[$j][$k])/($preferencia[$k] - $indiferencia[$k]);
                            }
                        }
                    }

                    $numerador = 0;
                    for ($l=0; $l < count($aux); $l++) {
                        $numerador = $numerador + ($peso[$l] * $aux[$l]);
                    }

                    $concordancia[$i][$j] = $numerador/$suma_pesos;
                }
            }
        }

        return $concordancia;
    }

    public function matriz_discordancia($m,$n,$matriz_desempeño,$preferencia,$veto){
        $discordancia = array();
        if (count($veto) == $n) {
            for ($i=0; $i < $m; $i++) { //selecciona la alternativa a comparar
                for ($j=0; $j < $m; $j++) { //selecciona las alternativas que seran comparadas
                    if ( $i == $j ) {//si las alternativas son iguales en ese punto el elemento de la concordancia sera 1
                        $discordancia[$i][$j] = "-";
                    }else{
                        $aux_dis = [];
                        for ($k=0; $k < $n; $k++) { //revolver de criterios
                            if ($matriz_desempeño[$i][$k] + $preferencia[$k] >= $matriz_desempeño[$j][$k]) {
                                $aux_dis[$k] = 0.00;
                            } else {
                                if ($matriz_desempeño[$i][$k]+$veto[$k] <= $matriz_desempeño[$j][$k]) {
                                    $aux_dis[$k] = 1.00;
                                } else {
                                    $aux_dis[$k] = ($matriz_desempeño[$j][$k] - $matriz_desempeño[$i][$k] - $preferencia[$k])/($veto[$k] - $preferencia[$k]);
                                }
                            }
                        }
                        $discordancia[$i][$j] = $aux_dis;
                    }
                }
            }
        } else {
            for ($i=0; $i < $m; $i++) {
                for ($j=0; $j < $m; $j++) {
                    $discordancia[$i][$j] = 0;
                }
            }
        }

        return $discordancia;
    }

    public function matriz_credibilidad($m,$concordancia,$discordancia){
        $credibilidad = array();
        for ($i=0; $i < $m; $i++) { //selecciona la alternativa a comparar
            for ($j=0; $j < $m; $j++) { //selecciona las alternativas que seran comparadas
                if ( $i == $j ) {//si las alternativas son iguales en ese punto el elemento de la concordancia sera 1
                    $credibilidad[$i][$j] = "-";
                }else{
                    $elem_discordancia = $discordancia[$i][$j];
                    $band = true;
                    for ($k=0; $k < count($elem_discordancia); $k++) { //revolver de criterios
                        if ($elem_discordancia[$k] <= $concordancia[$i][$j]) {
                            $band = true;
                        } else {
                            $band = false;
                            $k = count($elem_discordancia);
                        }
                    }

                    if ($band) {
                        $credibilidad[$i][$j] = $concordancia[$i][$j];
                    } else {
                        $acumulador = 1;
                        for ($k=0; $k < count($elem_discordancia); $k++) {
                            if ($elem_discordancia[$k] > $concordancia[$i][$j]) {
                                $acumulador = $acumulador * ((1-$elem_discordancia[$k])/(1-$concordancia[$i][$j]));
                            }
                        }
                        $credibilidad[$i][$j] = $concordancia[$i][$j]*$acumulador;
                    }
                }
            }
        }

        return $credibilidad;
    }

    public function primera_destilacion($proyectos,$credibilidad,$alfa,$beta)
    {
        $auxiliar = array();
        for ($i = 0; $i < count($credibilidad); $i++) {
            $auxiliar[$i] = max($credibilidad[$i]);
        }
        $lambda_max = max($auxiliar);

        $calificaciones_netas = $this->matrices_lambda($proyectos,$lambda_max,$credibilidad,$alfa,$beta);

        return $calificaciones_netas;
    }

    public function destilacion($proyectos,$credibilidad,$alfa,$beta){

        $nueva_credibilidad = array();

        for($i = 0; $i < count($proyectos); $i++){
            for($j = 0; $j < count($proyectos); $j++){
                if($i == $j){
                    $nueva_credibilidad[$i][$j] = "-";
                }else{
                    $nueva_credibilidad[$i][$j] = $credibilidad[$proyectos[$i]][$proyectos[$j]];
                }
            }
        }

        $auxiliar = array();
        for($i = 0; $i < count($nueva_credibilidad); $i++){
            $auxiliar[$i] = max($nueva_credibilidad[$i]);
        }

        $lambda_max = max($auxiliar);

        $calificaciones_netas = $this->matrices_lambda($proyectos,$lambda_max,$nueva_credibilidad,$alfa,$beta);

        return $calificaciones_netas;

    }

    public function matrices_lambda($proyectos,$lambda_max,$credibilidad,$alfa,$beta){

        $lambda = $lambda_max - ($beta + ($alfa * $lambda_max));

        $lambda_strength = array();
        $lambda_weakness = array();

        for ($i = 0; $i < count($proyectos); $i++) {
            for ($j = 0; $j < count($proyectos); $j++) {
                if ($i == $j) {
                    $lambda_strength[$i][$j] = "-";
                    $lambda_weakness[$i][$j] = "-";
                } else {
                    if ($credibilidad[$i][$j] > $lambda) {
                        $lambda_strength[$i][$j] = 1;
                    } else {
                        $lambda_strength[$i][$j] = 0;
                    }

                    if ((1 - ($beta + ($alfa * $lambda))) * $credibilidad[$i][$j] > $credibilidad[$j][$i]) {
                        $lambda_weakness[$i][$j] = 1;
                    } else {
                        $lambda_weakness[$i][$j] = 0;
                    }
                }
            }
        }

        $calificaciones_positivas = array();

        for ($i = 0; $i < count($proyectos); $i++) {
            $acumulador = 0;
            for ($j = 0; $j < count($proyectos); $j++) {
                if ($i != $j) {
                    $acumulador = $acumulador + $lambda_strength[$i][$j];
                }
            }
            $calificaciones_positivas[$i] = $acumulador;
        }

        $calificaciones_negativas = array();
        for ($i = 0; $i < count($proyectos); $i++) {
            $acumulador = 0;
            for ($j = 0; $j < count($proyectos); $j++) {
                if ($i != $j) {
                    $acumulador = $acumulador + $lambda_weakness[$j][$i];
                }
            }
            $calificaciones_negativas[$i] = $acumulador;
        }

        $calificaciones_netas = array();
        for ($i = 0; $i < count($proyectos); $i++) {
            $calificaciones_netas[$i] = [$calificaciones_positivas[$i] - $calificaciones_negativas[$i],'proyecto'=>$proyectos[$i]];
        }

        return $calificaciones_netas;
    }

    /*
     * Solo es aplicable para ordenar la primera destilacion
     * */
    public function ordena_primera_destilacion($destilacion){
        $temp = array();
        for($i = 1; $i < count($destilacion); $i++){
            for($j = 0; $j < $i; $j++){
                if($destilacion[$i][0] > $destilacion[$j][0]){
                    $temp[0] = $destilacion[$j];
                    $destilacion[$j] = $destilacion[$i];
                    $destilacion[$i] = $temp[0];
                }
            }
        }

        return $destilacion;
    }
}
