<?php
// Authentication routes...
Route::get('auth/login', [
    'uses' => 'Auth\AuthController@getLogin',
    'as'   => 'authLogin']);
Route::post('auth/login', [
    'uses' => 'Auth\AuthController@postLogin',
    'as'   => 'authPostLogin']);
Route::get('auth/logout', [
    'uses' => 'Auth\AuthController@getLogout',
    'as'   => 'authLogout']);

// Registration routes...
Route::get('auth/register', [
    'uses' => 'Auth\AuthController@getRegister',
    'as'   => 'authRegister']);
Route::post('auth/register', [
    'uses' => 'Auth\AuthController@postRegister',
    'as'   => 'authPostRegister']);

// Confirmacion de email
Route::get('auth/confirmacion/{email?}',[
    'uses' => 'Auth\AuthController@getConfirmation',
    'as'   => 'authConfirmation'
]);

/*Proyecto*/
Route::get('/',[
    'uses' => 'ElectreController@listarProyecto',
    'as'   => 'home'
]);

Route::get('mostrarProyecto/{id}',[
    'uses' => 'ElectreController@mostrarProyecto',
    'as'   => 'mostrarProyecto'
]);
/*------------------------------validado---------------------------*/
Route::group(['middleware'=>'auth'],function(){
    Route::get('crearProyecto',[
        'uses' => 'ElectreController@crearProyecto',
        'as'   => 'crearProyecto'
    ]);

    Route::get('misProyectos',[
        'uses' => 'ElectreController@listarMisProyectos',
        'as'   => 'misProyectos'
    ]);

    /*Elemento*/
    Route::get('elemento/{id}',[
        'uses' => 'ElectreController@crearElemento',
        'as'   => 'cargarElemento'
    ]);

    Route::post('almacenarElemento',[
        'uses' => 'ElectreController@almacenarElemento',
        'as'   => 'almacenarElemento'
    ]);

    Route::post('almacenarProyecto',[
        'uses' => 'ElectreController@almacenarProyecto',
        'as'   => 'almacenarProyecto'
    ]);

    Route::get('editarProyecto/{id}',[
        'uses' => 'ElectreController@editarProyecto',
        'as'   => 'editarProyecto'
    ]);

    Route::delete('eliminarProyecto/{id}',[
        'uses' => 'ElectreController@eliminarProyecto',
        'as'   => 'eliminarProyecto'
    ]);

    /*Matriz Desempeño*/
    Route::get('cargarDatos/{id}',[
        'uses' => 'ElectreController@cargarDatos',
        'as'   => 'cargarDatos'
    ]);

    Route::post('almacenarDatos',[
        'uses' => 'ElectreController@almacenarDatos',
        'as'   => 'almacenarDatos'
    ]);

    /*Rutas para editar*/

    Route::post('actualizarDatos',[
        'uses' => 'ElectreController@actualizarDatos',
        'as'   => 'actualizarDatos'
    ]);

    Route::post('actualizarElemento',[
        'uses' => 'ElectreController@actualizarElemento',
        'as'   => 'actualizarElemento'
    ]);

    Route::put('actualizarProyecto/{id}',[
        'uses' => 'ElectreController@actualizarProyecto',
        'as'   => 'actualizarProyecto'
    ]);

    Route::get('account',[
        'uses' => 'AccountController@index',
        'as'   => 'account'
    ]);

    Route::put('account/edit',[
        'uses' => 'AccountController@editAccount',
        'as'   => 'editAccount'
    ]);

    Route::get('account/users',[
       'uses' => 'AccountController@listAccount',
        'as'  => 'listAccount'
    ]);

    Route::put('user/edit/{id}',[
        'uses' => 'AccountController@editAccountUser',
        'as'   => 'editAccountUser'
    ]);

    Route::delete('users/delete/{id}',[
        'uses' => 'AccountController@deleteAccountUser',
        'as'   => 'deleteAccountUser'
    ]);
});

Route::get('xd',function(){
    return bcrypt('secret');
});

Route::get('asd','TestController@testeo');